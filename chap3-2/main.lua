local function adjustX(x)
  return math.max(-136, math.min(x, 136)) -- -136 <= x <= 136
end

local function adjustY(y)
  return math.max(16, math.min(y, 432)) -- 16 <= y <= 432
end

local function shouldPredict(obj_body, player_body)
  local prev_width, prev_height, prev_radius
  local size_rate = 10
  if obj_body.type ~= HitType.Circle then
    prev_width = player_body.width
    prev_height = player_body.height
    player_body.width = prev_width * size_rate
    player_body.height = prev_height * size_rate
  else
    prev_radius = player_body.radius
    player_body.radius = prev_radius * size_rate
  end
  local ret = hitTest(obj_body, player_body)
  if obj_body.type ~= HitType.Circle then
    player_body.width = prev_width
    player_body.height = prev_height
  else
    player_body.radius = prev_radius
  end
  return ret
end

local function calculateHitRisk(candidates, objects, player)
  for i, obj in ipairs(objects) do
    -- 1フレーム後のオブジェクトの座標を求める
    local obj_body = obj.hitBody
    if obj_body ~= nil then
      local player_body
      if obj_body.type == HitType.Circle then
        player_body = player.hitBodyCircle
      else
        player_body = player.hitBodyRect
      end
      if shouldPredict(obj_body, player_body) then
        for frame=1,5 do
          obj_body.x = obj_body.x + frame * obj.vx
          obj_body.y = obj_body.y + frame * obj.vy
          -- 自機の当たり判定はオブジェクトの当たり判定の種類によって使い分ける
          -- それぞれの移動操作でぶつかるかどうか調べる
          for j, cnd in ipairs(candidates) do
            player_body.x = adjustX(player.x + frame * cnd.dx)
            player_body.y = adjustY(player.y + frame * cnd.dy)
            if hitTest(player_body, obj_body) then
              cnd.hitrisk = cnd.hitrisk + (1/2)^frame
            end
          end
          -- 座標を元に戻す
          obj_body.x = obj.x 
          obj_body.y = obj.y 
          player_body.x = player.x
          player_body.y = player.y
        end
      end
    end
  end
end

local function generateCandidates(player)
  local candidates = {}
  local dxs = {0, 1, -1, 0, 0}
  local dys = {0, 0, 0, 1, -1}
  -- キー入力。停止、→、←、↓、↑
  local keys = {0x0,0x80,0x40,0x20,0x10}
  for i=1, #keys do
    candidates[i] = {
      key = keys[i],
      dx = player.speedFast * dxs[i],
      dy = player.speedFast * dys[i],
      followingCost = 0,
      hitrisk = 0
    }
  end
  return candidates
end

local function calculateFollowingCost(candidates, player, target)
  for i, cnd in ipairs(candidates) do
    local dx = 0
    if target ~= nil then
      dx = target.x - adjustX(player.x + cnd.dx)
    end
    cnd.followingCost = math.abs(dx)
  end
end

local function choosemin(candidates, func)
  local min = 99999999
  local min_i = -1
  for i, cnd in ipairs(candidates) do
    local v = func(cnd)
    if  v < min then
      min = v
      min_i = i
    end
  end
  return candidates[min_i]
end

local function findEnemyById(enemies, id)
  for i, enemy in ipairs(enemies) do
    if enemy.id == id then
      return enemy
    end
  end
  return nil
end

local target_enemy_id = nil
local function getCurrentTarget(player, enemies)
  local target_enemy = findEnemyById(enemies, target_enemy_id)
  if target_enemy == nil then
    target_enemy = choosemin(enemies, function(enemy)
      -- 幽霊や擬似的な敵やボスは無視
      if enemy.isSpirit or enemy.isPseudoEnemy or enemy.isBoss then
        return 99999999
      end
      return (enemy.x - player.x)^2
    end)
  end
  if target_enemy ~= nil then
    target_enemy_id = target_enemy.id
  else
    target_enemy_id = nil
  end
  return target_enemy
end

local function isNear(player, target)
  return (target.x - player.x)^2 < 32^2
end

local generateShootKey = coroutine.wrap(function ()
  while true do
    coroutine.yield(1)
    coroutine.yield(0)
  end
end)

function main()
  local myside = game_sides[player_side]
  local player = myside.player

  -- 移動候補
  local candidates = generateCandidates(player)
  -- 追尾する敵
  local target_enemy = getCurrentTarget(player, myside.enemies)
  -- 追尾コスト計算
  calculateFollowingCost(candidates, player, target_enemy)
  -- それぞれの移動操作の被弾リスクを計算
  calculateHitRisk(candidates, myside.enemies, player)
  calculateHitRisk(candidates, myside.bullets, player)
  calculateHitRisk(candidates, myside.exAttacks, player)
  -- 総合的なコストが最小の手を選ぶ
  local choice = choosemin(candidates, function (candidate)
    return candidate.hitrisk * 10000 + candidate.followingCost
  end)

  local should_charge = false
  if player.currentChargeMax > 200 and 
     player.currentCharge < 200 then
    should_charge = true
  end

  local keys = choice.key
  if should_charge then
    keys = keys + 1
  else
    -- 狙ってる敵と軸が合ったときに撃つ
    if target_enemy ~= nil and isNear(player, target_enemy) then
      keys = keys + generateShootKey()
    end
  end
  sendKeys(keys)
end

