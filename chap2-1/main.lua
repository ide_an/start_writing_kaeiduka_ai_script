local function generateCandidates(player)
  local candidates = {}
  local dxs = {0, 1, -1, 0, 0}
  local dys = {0, 0, 0, 1, -1}
  -- キー入力。停止、→、←、↓、↑
  local keys = {0x0,0x80,0x40,0x20,0x10}
  for i=1, #keys do
    candidates[i] = {
      key = keys[i],
      dx = player.speedFast * dxs[i],
      dy = player.speedFast * dys[i],
      hitrisk = 0
    }
  end
  return candidates
end

local function adjustX(x)
  return math.max(-136, math.min(x, 136)) -- -136 <= x <= 136
end

local function adjustY(y)
  return math.max(16, math.min(y, 432)) -- 16 <= y <= 432
end

local function calculateHitRisk(candidates, objects, player)
  for i, obj in ipairs(objects) do
    local obj_body = obj.hitBody
    if obj_body ~= nil then
      -- 1フレーム後のオブジェクトの座標を求める
      obj_body.x = obj_body.x + obj.vx
      obj_body.y = obj_body.y + obj.vy
      -- 自機の当たり判定はオブジェクトの当たり判定の種類によって使い分ける
      local player_body
      if obj_body.type == HitType.Circle then
        player_body = player.hitBodyCircle
      else
        player_body = player.hitBodyRect
      end
      -- それぞれの移動操作でぶつかるかどうか調べる
      for j, cnd in ipairs(candidates) do
        player_body.x = adjustX(player.x + cnd.dx)
        player_body.y = adjustY(player.y + cnd.dy)
        if hitTest(player_body, obj_body) then
          cnd.hitrisk = cnd.hitrisk + 1
        end
      end
      -- 座標を元に戻す
      obj_body.x = obj.x 
      obj_body.y = obj.y 
      player_body.x = player.x
      player_body.y = player.y
    end
  end
end

local function choose(candidates)
  local min_risk = 99999999
  local min_i = -1
  for i, cnd in ipairs(candidates) do
    if cnd.hitrisk < min_risk then
      min_risk = cnd.hitrisk
      min_i = i
    end
  end
  return candidates[min_i]
end

function main()
  local myside = game_sides[player_side]
  local player = myside.player
  -- 選択する移動操作の候補を生成
  local candidates = generateCandidates(player)
  -- それぞれの移動操作の被弾リスクを計算
  calculateHitRisk(candidates, myside.enemies, player)
  calculateHitRisk(candidates, myside.bullets, player)
  calculateHitRisk(candidates, myside.exAttacks, player)
  -- 被弾リスクが一番低い移動操作を選ぶ
  local choice = choose(candidates)
  -- キー入力として送信
  sendKeys(choice.key)
end
