local X_MAX = 136
local X_MIN = - X_MAX
local is_moving_right = true
function main()
  local my_side = game_sides[player_side]
  local player_x = my_side.player.x
  if(is_moving_right and X_MAX <= player_x) then
    -- 画面右端にぶつかったので切り返す
    is_moving_right = false
  elseif (not(is_moving_right) and player_x <= X_MIN) then
    -- 画面左端にぶつかったので切り返す
    is_moving_right = true
  end
  if (is_moving_right) then
    sendKeys(0x80) -- 右移動のキーを送る
  else
    sendKeys(0x40) -- 左移動のキーを送る
  end
end

